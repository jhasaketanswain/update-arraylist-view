package com.example.arraylist

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    private lateinit var enter: EditText
    private lateinit var btn: Button
    private lateinit var recyclerView: RecyclerView
    private var customAdapter: CustomAdapter?=null
    private var entryList: ArrayList<String>?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        enter=findViewById(R.id.enter)
        btn=findViewById(R.id.btn)
        recyclerView=findViewById(R.id.recyclerview)


        btn.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                callofCustomAdapter()
            }
        })
    }

    private fun callofCustomAdapter(){
        val value = enter.text.toString()
        Log.d("jhasaketan", "this is the value of enter in edittext : $value")

        entryList = ArrayList()
        entryList!!.clear()
        entryList!!.add(value)

        customAdapter = CustomAdapter(this, entryList!!)
        recyclerView.adapter = customAdapter
        recyclerView.layoutManager = LinearLayoutManager(this)
    }
}

