package com.example.arraylist

import android.content.Context
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class CustomAdapter(private val context : Context, private val arraylist : ArrayList<String>) : RecyclerView.Adapter<CustomAdapter.ViewHolder>(){

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textToShow = itemView.findViewById<TextView>(R.id.textToShow)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rootView : View = LayoutInflater.from(parent.context).inflate(R.layout.layout_custom_adapter, parent, false)
        return ViewHolder(rootView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val textPosition = arraylist[position]

        holder.textToShow.text = SpannableStringBuilder(textPosition)
    }

    override fun getItemCount(): Int {
        return arraylist.size
    }
}